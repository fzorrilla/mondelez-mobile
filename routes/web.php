<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
    return view('welcome_mobile');
})->name('welcome_mobile');

Route::post('/buscar_qr', 'BoardController@buscar_qr')->name('buscar_qr');
Route::post('/buscar_home', 'BoardController@buscar_home')->name('buscar_home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/login_mobile', function(){
    return view('login_mobile');
})->name('login_mobile');

Route::get('/mobileRecoverPass', function(){
    return view('mobileRecoverPass');
})->name('mobileRecoverPass');

Route::get('/logout', function(){
    Auth::logout();
    return Redirect::to('welcome_mobile');
});