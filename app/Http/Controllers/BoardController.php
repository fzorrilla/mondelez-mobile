<?php

namespace App\Http\Controllers;
use App\Board;
use App\User;
use App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use view;
class BoardController extends Controller
{

     public function buscar_qr(Request $request)
    {
      //  dd($request);
        $boards = Board::where('number', $request->codigo)->get();
        foreach ($boards as $key => $value) {
            $id = $value->id;
            $number = $value->number;
            $name = $value->name;
            $epps = $value->epps;
            $electric_system = $value->electric_system;
            $limit = $value->limit;
            $other = $value->other;
            $ing = $value->ing;
            $hse = $value->hse;
            $mant = $value->mant;
            $cal = $value->cal;
            $serv = $value->serv;
          }


        return view('tablero', compact(['number','name','epps','electric_system','limit','other','ing','hse','mant','cal','serv','id']));
    }
     public function buscar_home(Request $request)
    {
      //  dd($request);
        $boards = Board::where('number', $request->codigo)->get();
        foreach ($boards as $key => $value) {
            $id = $value->id;
            $number = $value->number;
            $name = $value->name;
            $epps = $value->epps;
            $electric_system = $value->electric_system;
            $limit = $value->limit;
            $other = $value->other;
            $ing = $value->ing;
            $hse = $value->hse;
            $mant = $value->mant;
            $cal = $value->cal;
            $serv = $value->serv;
          }
           return response()->json([
              'boards' => $boards
            ]);


        /*return json_encode('number','name','epps','electric_system','limit','other','ing','hse','mant','cal','serv','id');*/
    }

}
