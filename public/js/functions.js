function scrollDown() {
  var sectionTo = $(this).attr('href');
  var len = $(this).attr('id').length-1;
  var last = $(this).attr('id').charAt(len);
  // console.log(len);
  // console.log(last);

  $('#chatbot-' + last).show();

  $('html, body').animate({
    scrollTop: $(sectionTo).offset().top
  }, 1000);
}

function scrollDown2(elm) {
  var sectionTo = elm.attr('href');
  var len = elm.attr('id').length-1;
  var last = elm.attr('id').charAt(len);
  // console.log(len);
  // console.log(last);

  $('#chatbot-' + last).show();

  $('html, body').animate({
    scrollTop: $(sectionTo).offset().top
  }, 1000);
}

