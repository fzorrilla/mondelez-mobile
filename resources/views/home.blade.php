@extends('layouts.mob')

@section('title', 'Lima Lab Consulting Group')

@section('content')

<div class="container container-mobile">

    <div class="container">
      <div class="row">
        <div class="col-md-8 pb-4 pr-0" style="width: 100%;text-align: left;">
          <br>
          <div class="message-welcome-plan" id="tablero">
          </div>
          <div class="message-welcome-plan" id="nombre">
          </div>
           <div class="message-welcome-plan" id="epps">
          </div>
          <div class="message-welcome-plan" id="sistema_electrico">
          </div>
          <div class="message-welcome-plan" id="limit">
          </div>
          <div class="message-welcome-plan" id="other">
          </div>
          <div class="message-welcome-plan" id="hse">
          </div>
          <div class="message-welcome-plan" id="ing">
          </div>
          <div class="message-welcome-plan" id="mant">
          </div>
          <div class="message-welcome-plan" id="cal">
          </div>
          <div class="message-welcome-plan" id="serv">
            <div id="servicio_img"></div>
          </div>
        </div>
      </div>
      <div class="col-md-8 pb-4 pr-0" style="width: 100%;text-align: left;">
          <div class="message-welcome-plan">
            <i class="fa fa-unlock" aria-hidden="true"> Público</i>
          </div>
        </div>
      <div class="row mt-1">
        <div class="col-md-6 pb-4" style="width: 25%">
            <a id="epps">
              <img src="{{ asset('img/epps.png') }}" style="width: 72px;height: 72px;text-align: center;">
            </a>
            <div class="col-md-12 text-purple pl-0 pr-0">
              <center>EPPs</center>
            </div>
        </div>
         <div class="col-md-6 pb-4" style="width: 25%">
            <a id="sistema_electrico">
              <img src="{{ asset('img/sistema_electrico.png') }}" style="width: 72px;height: 72px;text-align: center;">
            </a>
            <div class="col-md-12 text-purple pl-0 pr-0">
              <center>Sistema Eléctrico</center>
            </div>
        </div>
        <div class="col-md-6 pb-4" style="width: 25%">
            <a id="limite">
              <img src="{{ asset('img/limite.png') }}" style="width: 72px;height: 72px;text-align: center;">
            </a>
            <div class="col-md-12 text-purple pl-0 pr-0">
              <center>Límites</center>
            </div>
        </div>
         <div class="col-md-6 pb-4" style="width: 25%">
            <a id="other">
              <img src="{{ asset('img/other.png') }}" style="width: 72px;height: 72px;text-align: center;">
            </a>
            <div class="col-md-12 text-purple pl-0 pr-0">
              <center>Otro</center>
            </div>
        </div>
      </div>
      <div class="col-md-8 pb-4 pr-0" style="width: 100%;text-align: left;">
          <div class="message-welcome-plan">
            <i class="fa fa-lock" aria-hidden="true"> Privados</i>
          </div>
        </div>
      <div class="row mt-1">
         <div class="col-md-6 pb-4" style="width: 25%">
            <a id="ing">
              <img src="{{ asset('img/ingenieria.png') }}" style="width: 72px;height: 72px;text-align: center;">
            </a>
            <div class="col-md-12 text-purple pl-0 pr-0">
              <center>Ingenieria</center>
            </div>
        </div>
         <div class="col-md-6 pb-4" style="width: 25%">
            <a id="hse">
              <img src="{{ asset('img/hse.png') }}" style="width: 72px;height: 72px;text-align: center;">
            </a>
            <div class="col-md-12 text-purple pl-0 pr-0">
              <center>HSE</center>
            </div>
        </div>
        <div class="col-md-6 pb-4" style="width: 25%">
            <a id="cal">
              <img src="{{ asset('img/calidad.png') }}" style="width: 72px;height: 72px;text-align: center;">
            </a>
            <div class="col-md-12 text-purple pl-0 pr-0">
              <center>Calidad</center>
            </div>
        </div>
         <div class="col-md-6 pb-4" style="width: 50%">
            <a id="mant">
              <img src="{{ asset('img/mantenimiento.png') }}" style="width: 72px;height: 72px;text-align: center;">
            </a>
            <div class="col-md-12 text-purple pl-0 pr-0">
              <center>Mantenimiento</center>
            </div>
        </div>
          <div class="col-md-6 pb-4" style="width: 50%">
            <a id="serv">
              <img src="{{ asset('img/servicio.png') }}" style="width: 72px;height: 72px;text-align: center;">
            </a>
            <div class="col-md-12 text-purple pl-0 pr-0">
              <center>Servicio</center>
            </div>
        </div>
      </div>
    </div>
</div>
@stop
@section('js')
<script type="text/javascript">
  $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  $.ajax({
    type : 'post',
     url : 'buscar_home',
     data : { "codigo" : localStorage.getItem("codigo")  },
     success : function(data, textStatus, jqXHR)
      {
        $.each(data, function(i, item) {
          $("#tablero").text('Tablero: ' + item[0].number ),
          $("#nombre").text('Nombre: ' + item[0].name ) ,
          $("#epps").attr("href","/mondelez-mobile/public/storage/"+ item[0].epps),
          $("#sistema_electrico").attr("href","/mondelez-mobile/public/storage/"+ item[0].electric_system),
          $("#limit").attr("href","/mondelez-mobile/public/storage/"+ item[0].limit),
          $("#other").attr("href","/mondelez-mobile/public/storage/"+ item[0].other),
          $("#ing").attr("href","/mondelez-mobile/public/storage/"+ item[0].ing),
          $("#hse").attr("href","/mondelez-mobile/public/storage/"+ item[0].hse),
          $("#mant").attr("href","/mondelez-mobile/public/storage/"+ item[0].mant),
          $("#cal").attr("href","/mondelez-mobile/public/storage/"+ item[0].cal),
          $("#serv").attr("href","/mondelez-mobile/public/storage/"+ item[0].serv),

        });

       },
      error : function(XMLHttpRequest, textStatus, errorThrown)
       {  },
     }
     );
</script>
@stop
