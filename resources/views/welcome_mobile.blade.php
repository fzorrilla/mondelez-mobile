@extends('layouts.mob')

@section('title', 'Lima Lab Consulting Group')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
@section('content')

<div class="container container-mobile pb-0 pt-0">
    <br>
    <p><b><h3>Escanear Código QR</h3><b></p>

     <div class="col-md-12 contain-img-icon">
        <video id="preview" style="width: auto; height: auto; margin-left: -30px; margin-top: 50px;"></video>
      </div>
      <form id="form" action="buscar_qr" method="post">
        <input type="hidden" name="codigo" id="codigo" value="">
        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
      </form>
    <!--<div class="col-md-12 pt-5">

        <a href="{{-- route('buscar_qr', '1') --}}">
          <input type="button" value="EMPECEMOS" class="socorro__card-button button-mobile-socorro" >
        </a>
    </div>-->
</div>
    <script type="text/javascript">
      let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
      scanner.addListener('scan', function (content) {
        alert(content);
        $("#codigo").val(content);

        localStorage.setItem("codigo",content);
        $("#form").submit();

      });
      Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
          scanner.start(cameras[0]);
        } else {
          console.error('No cameras found.');
        }
      }).catch(function (e) {
        console.error(e);
      });
    </script>

@stop

@section('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
	<script type="text/javascript">


	</script>
@stop
